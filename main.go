package main

import "log"

var version string

func main() {
	log.Printf("Hello semantic world! This is version %s", determineVersion())
}

func determineVersion() string {
	if len(version) == 0 {
		return "[unknown]"
	}
	return version
}
