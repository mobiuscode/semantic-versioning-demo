# Semantic Versioning Demo

A demo project showing how to use [Semantic Release](https://github.com/semantic-release/semantic-release) to automate
[Semantic Versioning](https://semver.org) in a GitLab project.